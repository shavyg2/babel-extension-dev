var extension_loader = require("require-extension/extension-loader").default;
"use strict";
var log = require("debug")("node.debug.babel");

module.exports = function (options) {
  require.extensions[".js"] = function (content, filename) {
    log("loading");
    var babel = require("babel-core");
    var out = babel.transform(content, options);
    return out.code;
  };
};
